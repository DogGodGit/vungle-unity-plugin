using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// 为确保 Vungle SDK 的正确行为，请在 Unity 编辑器中针对 iOS、Android 或 Windows 平台。
public class TitleGUI : MonoBehaviour
{
#if UNITY_IPHONE || UNITY_ANDROID || UNITY_WSA_10_0 || UNITY_WINRT_8_1 || UNITY_METRO

    public Button playPlacement1Button;

    public Button loadPlacement2Button;

    public Button playPlacement2Button;

    public Button loadPlacement3Button;

    public Button playPlacement3Button;

    public Button toBannersCanvas;

    public Text appIDText;

    public Text sdkInitText;

    public Text placementID1Text;

    public Text placementID2Text;

    public Text placementID3Text;

    // 这些 PlacementID 指向仪表板上的 Vungle 测试应用程序。
    // 将这些替换为您自己的 PlacementID 以测试您的展示位置的仪表板设置。
    private List<string> placementLists = new List<string>
    {
#if UNITY_IPHONE
    	"DEFAULT-2227894",
		"REWARDED02-4065911",
		"REWARDED_PLAYABLE01-4686142",
#elif UNITY_ANDROID
		"DEFAULT-6595425",
		"DYNAMIC_TEMPLATE_INTERSTITIAL-6969365",
		"DYNAMIC_TEMPLATE_REWARDED-5271535"
#elif UNITY_WSA_10_0 || UNITY_WINRT_8_1 || UNITY_METRO
        "DEFAULT-4825113",
        "PLACEME-1604705",
        "2-8749130",
    };

#endif

    private void Start()
    {
        SetupButtonsAndText();
    }

    private void Update()
    {
        updateButtonState();
        sdkInitText.text = $"Sdk Version {Vungle.PluginVersion} Init: {Vungle.isInitialized()}";
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            Vungle.onPause();
        }
        else
        {
            Vungle.onResume();
        }
    }

    // UI initialization
    private void SetupButtonsAndText()
    {
        if (!ViewManager.Instance) return;
        foreach (var item in placementLists)
        {
            ViewManager.Instance.SetPlacementStatus(item, false);
        }

        appIDText.text = "App ID: " + ViewManager.Instance.appID;

        placementID1Text.text = "Placement ID: " + placementLists[0];
        placementID2Text.text = "Placement ID: " + placementLists[1];
        placementID3Text.text = "Placement ID: " + placementLists[2];

        playPlacement1Button.onClick.AddListener(onPlayPlacement1);
        loadPlacement2Button.onClick.AddListener(onLoadPlacement2);
        playPlacement2Button.onClick.AddListener(onPlayPlacement2);
        loadPlacement3Button.onClick.AddListener(onLoadPlacement3);
        playPlacement3Button.onClick.AddListener(onPlayPlacement3);
        toBannersCanvas.onClick.AddListener(showBannerCanvas);
        toBannersCanvas.interactable = true;
    }

    private void onPlayPlacement1()
    {
        Vungle.playAd(placementLists[0]);
    }

    private void onLoadPlacement2()
    {
        Vungle.loadAd(placementLists[1]);
    }

    private void onPlayPlacement2()
    {
        // option to change orientation
        Dictionary<string, object> options = new Dictionary<string, object>();
#if UNITY_IPHONE
		options["orientation"] = 6;
#else
        options["orientation"] = true;
#endif


        Vungle.playAd(options, placementLists[1]);
    }

    private void onLoadPlacement3()
    {
        Vungle.loadAd(placementLists[2]);
    }

    private void onPlayPlacement3()
    {
        // 自定义警报窗口和发送 user_id 的选项
        Dictionary<string, object> options = new Dictionary<string, object>
        {
            ["userTag"] = ViewManager.Instance.userID,
            ["alertTitle"] = "Title",
            ["alertText"] = "Alert",
            ["closeText"] = "Close",
            ["continueText"] = "Continue",
            ["ordinal"] = "77777"
        };

        Vungle.playAd(options, placementLists[2]);
    }

    private void showBannerCanvas()
    {
        ViewManager.Instance.LoadView(ViewManager.ViewId.BannerAd);
    }

    private void updateButtonState()
    {
        if (!ViewManager.Instance) return;
        playPlacement1Button.interactable = ViewManager.Instance.Placements[placementLists[0]];
        loadPlacement2Button.interactable = !ViewManager.Instance.Placements[placementLists[1]];
        playPlacement2Button.interactable = ViewManager.Instance.Placements[placementLists[1]];
        loadPlacement3Button.interactable = !ViewManager.Instance.Placements[placementLists[2]];
        playPlacement3Button.interactable = ViewManager.Instance.Placements[placementLists[2]];
    }

#endif
}