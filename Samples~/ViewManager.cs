using System.Collections.Generic;
using UnityEngine;

public class ViewManager : MonoBehaviour
{
    // 这些 AppID 指向仪表板上的 Vungle 测试应用程序。
    // 将这些替换为您自己的 AppID 以测试您应用的仪表板设置。
    private string iOSAppID = "6142c7205409d34e956127d3";

    private string androidAppID = "6142c6b78f6c895c5b0afa94";

    private string windowsAppID = "6142c5677ab6ed1ded5a0e1d";

    public string userID = "6142c4c539268a0017c8713f";

    public string appID
    {
        get
        {
            return
#if UNITY_IPHONE
		    iOSAppID;
#elif UNITY_ANDROID
		    androidAppID;
#elif UNITY_WSA_10_0 || UNITY_WINRT_8_1 || UNITY_METRO
            windowsAppID;
#else
            "";
#endif
        }
    }

    public Dictionary<string, bool> Placements = new Dictionary<string, bool>();

    public Canvas[] canvases;

    private Canvas activeCanvas;

    private static ViewManager mInstance;

    public static ViewManager Instance
    {
        get
        {
            return mInstance;
        }
    }

    private void Awake()
    {
        if (mInstance != null)
        {
            Destroy(this);
            return;
        }
        mInstance = this;

        // 在 init 之前附加事件处理程序。
        initializeEventHandlers();
    }

    private void Start()
    {
        LoadView(ViewId.FullAd);
    }

    public void LoadView(ViewId id)
    {
        if (activeCanvas != null)
        {
            activeCanvas.gameObject.SetActive(false);
        }
        canvases[(int)id].gameObject.SetActive(true);
        activeCanvas = canvases[(int)id];
    }

    /// <summary>
    /// 为所有可用的 Vungle 事件设置 EventHandler
    /// </summary>
    private void initializeEventHandlers()
    {
        DebugLog("begin - SDK initialized");
        Vungle.setLogEnable(false);

        // 从sdk触发初始化事件
        Vungle.onInitializeEvent += () =>
        {
            DebugLog("onInitializeEvent - SDK initialized");

            string message = "GDPR_message-version";
            Vungle.updateConsentStatus(Vungle.Consent.Denied, message);
            Vungle.updateCCPAStatus(Vungle.Consent.Denied);
        };

        // 广告即将播放时触发的事件
        Vungle.onAdStartedEvent += (placementID) =>
        {
            DebugLog("onAdStartedEvent " + placementID + " is starting!  Pause your game  animation or sound here.");
        };

        // 当广告的可播放状态改变时触发事件
        // 它可用于启用某些功能，只有在广告播放可用时才能访问
        Vungle.adPlayableEvent += (placementID, adPlayable) =>
        {
            DebugLog("adPlayableEvent - Ad's playable state has been changed! placementID " + placementID + ". Now: " + adPlayable);
            SetPlacementStatus(placementID, adPlayable);
        };

        // 当 Vungle 广告完成并提供有关此事件的完整信息时触发事件
        // 这些可用于确定用户观看了多少视频，他们是否提前跳过了广告等。
        Vungle.onAdFinishedEvent += (placementID, args) =>
        {
            DebugLog("onAdFinishedEvent - GDPR " + Vungle.getCCPAStatus());
            DebugLog("onAdFinishedEvent - CCPA " + Vungle.getConsentStatus());
            DebugLog("onAdFinishedEvent - placementID " + placementID + ", was call to action clicked:" + args.WasCallToActionClicked + ", is completed view:" + args.IsCompletedView);

            SetPlacementStatus(placementID, false);
        };

        // Other events
        Vungle.onLogEvent += (log) =>
        {
            DebugLog("onLogEvent - Log: " + log);
        };

        Vungle.onAdClickEvent += (placementID) =>
        {
            DebugLog("onClick - Log: " + placementID);
        };

        Vungle.onAdRewardedEvent += (placementID) =>
        {
            DebugLog("onAdRewardedEvent - Log: " + placementID);
        };

        Vungle.onAdEndEvent += (placementID) =>
        {
            DebugLog("onAdEnd - Log: " + placementID);
        };

        Vungle.onErrorEvent += (placementID) =>
        {
            DebugLog("onErrorEvent - Log: " + placementID);
        };

        Vungle.onWarningEvent += (placementID) =>
        {
            DebugLog("onWarningEvent - Log: " + placementID);
        };

        try
        {
            // 自 6.3.0 起，Vungle Unity 插件在启动时不再需要放置 ID
            Vungle.init(appID);
        }
        catch (System.Exception e)
        {
            DebugLog("Exception - Log: " + e.Message);
        }
    }

    public void SetPlacementStatus(string placementID, bool adPlayable)
    {
        if (Placements.ContainsKey(placementID))
            Placements[placementID] = adPlayable;
        else
            Placements.Add(placementID, adPlayable);
    }

    /// <summary>
    /// 确保日志消息格式相同的常用方法
    /// </summary>
    /// <param name="message"></param>
    private void DebugLog(string message)
    {
        Debug.Log("VungleUnity" + System.DateTime.Now + ": " + message);
    }

    public enum ViewId
    {
        FullAd = 0,

        BannerAd = 1,
    }
}