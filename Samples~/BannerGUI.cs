using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BannerGUI : MonoBehaviour
{
#if UNITY_IPHONE || UNITY_ANDROID || UNITY_WSA_10_0 || UNITY_WINRT_8_1 || UNITY_METRO

    // 这些 PlacementID 指向仪表板上的 Vungle 测试应用程序。
    // 将这些替换为您自己的 PlacementID 以测试您的展示位置的仪表板设置。
    private List<string> placementLists = new List<string>
    {
#if UNITY_IPHONE
    	"BANNER04-8166553",
		"MREC03-4489762",
		"BANNER05-4730786",
#elif UNITY_ANDROID
		"BANNER-5454585",
		"MREC-2191415",
		"BANNER2-6990728"
#elif UNITY_WSA_10_0 || UNITY_WINRT_8_1 || UNITY_METRO
        "BANNER-1056928",
        "MREC-8367079",
        "BANNER-4486585",
    };

#endif

    public Button loadBanner1Button;

    public Button playBanner1Button;

    public Button loadMrec1Button;

    public Button playMrec1Button;

    public Button loadBanner2Button;

    public Button playBanner2Button;

    public Button closeBanner1Button;

    public Button closeMrec1Button;

    public Button closeBanner2Button;

    public Button toFullscreenButton;

    public Text placementID1Text;

    public Text placementID2Text;

    public Text placementID3Text;

    private bool adInited = true;

    private string appID;

    // Use this for initialization
    private void Start()
    {
        Setup();
    }

    // Update is called once per frame
    private void Update()
    {
        updateButtonState();
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            Vungle.onPause();
        }
        else
        {
            Vungle.onResume();
        }
    }

    private void Setup()
    {
        if (!ViewManager.Instance) return;
        foreach (var item in placementLists)
        {
            ViewManager.Instance.SetPlacementStatus(item, false);
        }

        placementID1Text.text = "Placement ID: " + placementLists[0];
        placementID2Text.text = "Placement ID: " + placementLists[1];
        placementID3Text.text = "Placement ID: " + placementLists[2];

        toFullscreenButton.onClick.AddListener(showFullscreenCanvas);
        loadBanner1Button.onClick.AddListener(onLoadBanner1);
        loadMrec1Button.onClick.AddListener(onLoadMrec1);
        loadBanner2Button.onClick.AddListener(onLoadBanner2);
        playBanner1Button.onClick.AddListener(onPlayBanner1);
        playMrec1Button.onClick.AddListener(onPlayMrec1);
        playBanner2Button.onClick.AddListener(onPlayBanner2);
        closeBanner1Button.onClick.AddListener(onCloseBanner1);
        closeMrec1Button.onClick.AddListener(onCloseMrec1);
        closeBanner2Button.onClick.AddListener(onCloseBanner2);

        closeBanner1Button.interactable = false;
        closeMrec1Button.gameObject.SetActive(false);
        closeBanner2Button.interactable = false;
    }

    private void onLoadBanner1()
    {
        Vungle.loadBanner(placementLists[0], Vungle.VungleBannerSize.VungleAdSizeBanner, Vungle.VungleBannerPosition.TopCenter);
    }

    private void onPlayBanner1()
    {
        Vungle.showBanner(placementLists[0]);
        closeBanner1Button.interactable = true;
    }

    private void onLoadMrec1()
    {
        Vungle.loadBanner(placementLists[1], Vungle.VungleBannerSize.VungleAdSizeBannerMedium, Vungle.VungleBannerPosition.Centered);
    }

    private void onPlayMrec1()
    {
        Vungle.showBanner(placementLists[1]);
        closeMrec1Button.gameObject.SetActive(true);
    }

    private void onLoadBanner2()
    {
        Vungle.loadBanner(placementLists[2], Vungle.VungleBannerSize.VungleAdSizeBannerShort, Vungle.VungleBannerPosition.BottomCenter);
    }

    private void onPlayBanner2()
    {
        Vungle.showBanner(placementLists[2]);
        closeBanner2Button.interactable = true;
    }

    private void onCloseBanner1()
    {
        Vungle.closeBanner(placementLists[0]);
        closeBanner1Button.interactable = false;
    }

    private void onCloseMrec1()
    {
        Vungle.closeBanner(placementLists[1]);
        closeMrec1Button.gameObject.SetActive(false);
    }

    private void onCloseBanner2()
    {
        Vungle.closeBanner(placementLists[2]);
        closeBanner2Button.interactable = false;
    }

    private void showFullscreenCanvas()
    {
        ViewManager.Instance.LoadView(ViewManager.ViewId.FullAd);
        ViewManager.Instance.canvases[1].gameObject.SetActive(false);
    }

    private void updateButtonState()
    {
        if (!ViewManager.Instance) return;
        loadBanner1Button.interactable = !ViewManager.Instance.Placements[placementLists[0]];
        playBanner1Button.interactable = ViewManager.Instance.Placements[placementLists[0]];
        loadMrec1Button.interactable = !ViewManager.Instance.Placements[placementLists[1]];
        playMrec1Button.interactable = ViewManager.Instance.Placements[placementLists[1]];
        loadBanner2Button.interactable = !ViewManager.Instance.Placements[placementLists[2]];
        playBanner2Button.interactable = ViewManager.Instance.Placements[placementLists[2]];
    }

#endif
}